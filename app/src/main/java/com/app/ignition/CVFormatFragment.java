package com.app.ignition;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class CVFormatFragment extends Fragment implements View.OnClickListener {

    private Context mContext;
    private TextView txtSelectCvFormat;
    private TextView btnSubmit;

    public static Fragment newInstance() {
        CVFormatFragment fragment = new CVFormatFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_cv_formats_list, container, false);
        CommonUtils.hideSoftKeyboard(getActivity());
        setToolBar(rootView);
        findViews(rootView);
        init();
        return rootView;
    }

    private void setToolBar(View rootView) {
    }

    private void findViews(View rootView) {
        btnSubmit = (TextView) rootView.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
    }

    private void init() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                CommonUtils.addFragmentTransaction((AppCompatActivity) mContext, HomeFragment.newInstance(),
                        HomeFragment.class.getSimpleName());
                break;
        }
    }
}