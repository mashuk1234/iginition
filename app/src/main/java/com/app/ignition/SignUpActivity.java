package com.app.ignition;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import org.json.JSONObject;

public class SignUpActivity extends AppCompatActivity implements OnClickListener {
    private static final String TAG = SignUpActivity.class.getSimpleName();
    private TextInputLayout mTInputEmail;
    private TextInputLayout mTInputName;
    private TextInputLayout mTInputPassword;
    private TextInputLayout mTInputPhone;
    private TextView mTvSignIn;
    private TextView mTvSignUp;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_tone);

        getSupportActionBar().setTitle(getResources().getString(R.string.signup));
        setToolBar();
        findViews();
        setListeners();
    }

    private void setToolBar() {
        /*((TextView) findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.signup));
        ImageView mIvBack = (ImageView) findViewById(R.id.imgBack);
        mIvBack.setVisibility(0);
        mIvBack.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                CommonUtils.hideSoftKeyboard(SignUpActivity.this);
                SignUpActivity.onBackPressed();
            }
        });
        ((ImageView) findViewById(R.id.imgEdit)).setVisibility(8);
        findViewById(R.id.mFrameToolbarCart).setVisibility(8);*/
    }

    private void findViews() {
        mTInputName = (TextInputLayout) findViewById(R.id.tInputName);
        mTInputEmail = (TextInputLayout) findViewById(R.id.tInputEmail);
        mTInputPassword = (TextInputLayout) findViewById(R.id.tInputPassword);
        mTInputPhone = (TextInputLayout) findViewById(R.id.tInputPhoneNumber);
        mTvSignUp = (TextView) findViewById(R.id.tvSignUp);
        mTvSignIn = (TextView) findViewById(R.id.tvSignIn);
    }

    private void setListeners() {
        mTvSignUp.setOnClickListener(this);
        mTvSignIn.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSignIn /*2131231170*/:
                finish();
                return;
            case R.id.tvSignUp /*2131231171*/:
                validateSignUpData();
                return;
            default:
                return;
        }
    }

    private void validateSignUpData() {

    }

}
