package com.app.ignition;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by mashuk on 28-Oct-18.
 */
public class FreshersActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout linLayPersonalInfo;
    private LinearLayout linLayPersonalInfoDetails;
    private LinearLayout linLayEducation;
    private LinearLayout linLayEducationDetails;
    private LinearLayout linLayProjectDetails;
    private LinearLayout linLayProjectSubDetails;
    private LinearLayout linLayTechnicalSkills;
    private LinearLayout linLayTechnicalSkillsDetails;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freshers);
        findViews();
    }

    private void findViews() {
        linLayPersonalInfo = (LinearLayout) findViewById(R.id.linlay_personal_info);
        linLayPersonalInfoDetails = (LinearLayout) findViewById(R.id.linlay_personal_info_details);
        linLayEducation = (LinearLayout) findViewById(R.id.linlay_education);
        linLayEducationDetails = (LinearLayout) findViewById(R.id.linlay_education_details);
        linLayProjectDetails = (LinearLayout) findViewById(R.id.linlay_project_details);
        linLayProjectSubDetails = (LinearLayout) findViewById(R.id.linlay_project_sub_details);
        linLayTechnicalSkills = (LinearLayout) findViewById(R.id.linlay_technical_skills);
        linLayTechnicalSkillsDetails = (LinearLayout) findViewById(R.id.linlay_technical_skills_details);

        linLayPersonalInfo.setOnClickListener(this);
        linLayEducation.setOnClickListener(this);
        linLayProjectDetails.setOnClickListener(this);
        linLayTechnicalSkills.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linlay_personal_info:
                if (linLayPersonalInfoDetails.getVisibility() == View.VISIBLE) {
                    linLayPersonalInfoDetails.setVisibility(View.GONE);
                } else {
                    linLayPersonalInfoDetails.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.linlay_education:
                if (linLayEducationDetails.getVisibility() == View.VISIBLE) {
                    linLayEducationDetails.setVisibility(View.GONE);
                } else {
                    linLayEducationDetails.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.linlay_project_details:
                if (linLayProjectSubDetails.getVisibility() == View.VISIBLE) {
                    linLayProjectSubDetails.setVisibility(View.GONE);
                } else {
                    linLayProjectSubDetails.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.linlay_technical_skills:
                if (linLayTechnicalSkillsDetails.getVisibility() == View.VISIBLE) {
                    linLayTechnicalSkillsDetails.setVisibility(View.GONE);
                } else {
                    linLayTechnicalSkillsDetails.setVisibility(View.VISIBLE);
                }
                break;
        }
    }
}
