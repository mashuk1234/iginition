package com.app.ignition;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PaymentFragment extends Fragment implements View.OnClickListener {

    private Context mContext;

    public static Fragment newInstance() {
        PaymentFragment fragment = new PaymentFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_payment, container, false);
        CommonUtils.hideSoftKeyboard(getActivity());
        setToolBar(rootView);
        findViews(rootView);
        init();
        return rootView;
    }

    private void setToolBar(View rootView) {
    }

    private void findViews(View rootView) {

    }

    private void init() {

    }

    @Override
    public void onClick(View v) {

    }
}