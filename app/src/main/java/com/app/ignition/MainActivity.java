package com.app.ignition;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private boolean mDoubleBackToExitPressedOnce = false;
    private Button mBtnFreshers;
    private RelativeLayout mMainView;
    private NavigationView mNavigationView;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        mNavigationView = findViewById(R.id.nav_view);

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle1 = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle1);
        toggle1.syncState();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, 0);
                mMainView.setTranslationX(slideOffset * drawerView.getWidth());
                // Apply animation to image view
                drawer.bringChildToFront(drawerView);
                drawer.requestLayout();
            }
        };

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        findViews();

        setDefaultFragment();
    }

    private void setDefaultFragment() {
        CommonUtils.replaceTabContainerFragment(MainActivity.this, HomeFragment.newInstance(),
                HomeFragment.class.getSimpleName());
    }

    private void findViews() {
        //Interstitial Ad
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));
        displayInterstitialAd();
    }

    private void displayInterstitialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }
        });
    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            quitApp();
        }
    }

    private void quitApp() {
        if (mDoubleBackToExitPressedOnce) {
            finish();
        }

        if (!mDoubleBackToExitPressedOnce) {
            this.mDoubleBackToExitPressedOnce = true;
            Toast.makeText(MainActivity.this, R.string.toast_press_to_close, Toast.LENGTH_SHORT).show();
        }

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                mDoubleBackToExitPressedOnce = false;
            }
        }, 1900);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            /*new Handler().postDelayed(new Runnable() {
                public void run() {
                    CommonUtils.replaceTabContainerFragment(MainActivity.this, HomeFragment.newInstance(),
                            HomeFragment.class.getSimpleName());
                }
            }, 1800);*/
            CommonUtils.replaceTabContainerFragment(MainActivity.this, HomeFragment.newInstance(),
                    HomeFragment.class.getSimpleName());

        } else if (id == R.id.nav_request_free_cv) {
            CommonUtils.replaceTabContainerFragment(MainActivity.this, RequestCVFragment.newInstance(),
                    RequestCVFragment.class.getSimpleName());

        } else if (id == R.id.nav_add_project) {
            CommonUtils.replaceTabContainerFragment(MainActivity.this, PostProjectIdeaFragment.newInstance(),
                    PostProjectIdeaFragment.class.getSimpleName());

        } else if (id == R.id.nav_get_a_project_idea) {
            CommonUtils.replaceTabContainerFragment(MainActivity.this, ProjectsListFragment.newInstance(),
                    ProjectsListFragment.class.getSimpleName());

        } else if (id == R.id.nav_news_feeds) {

        } else if (id == R.id.nav_contact_us) {

        } else if (id == R.id.nav_logout) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
