package com.app.ignition;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class SplashActivity extends AppCompatActivity {
    private Handler mHandler = new Handler();
    private Runnable mRunnable;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonUtils.hideStatusBar(this);
        Log.e("sdfsad", "onCreate: " + SplashActivity.class.getName());
        setContentView(R.layout.activity_splash_tone);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                 /*   if (Prefrences.getInt(SplashActivity.this, Prefrences.USER_ID) == 0) {
                        SplashActivity.this.startActivity(new Intent(SplashActivity.this, Class.forName(AppNavigation.strLoginScreen)));
                    } else {
                        SplashActivity.this.startActivity(new Intent(SplashActivity.this, Class.forName(AppNavigation.strMainOneScreen)));
                    }*/
                SplashActivity.this.finish();
            }
        }, 2000);
    }

    protected void onDestroy() {
        if (!(this.mHandler == null || this.mRunnable == null)) {
            this.mHandler.removeCallbacks(this.mRunnable);
        }
        super.onDestroy();
    }
}
