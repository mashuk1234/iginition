package com.app.ignition;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

public class CommonUtils {
    public static final Pattern EMAIL_PATTERN = Pattern.compile("^[A-Za-z0-9-]+[_A-Za-z0-9-]*(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    public static final Pattern FIRST_LAST_NAME_PATTERN = Pattern.compile("^[A-Za-z0-9]+[A-Za-z-\\.\\-\\_\\']*$");
    public static final Pattern INVALID_EMAIL_PATTERN = Pattern.compile("^[0-9-]+[_0-9-]*(\\.[_0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    public static final Pattern PASSWORD_VALIDATION = Pattern.compile("[A-Za-z0-9\\@\\#\\_\\'\\^\\*\\=\\:\\-\\+\\`]+$");
    public static final int REQUEST_CAMERA = 1234;
    public static final int SELECT_FILE = 4321;
    public static final DecimalFormat decimalFormat = new DecimalFormat("0.00");
    public static Uri imageURI;
    public static String mCurrentPhotoPath;
    private static File mFinalFile;

    public static void setStatusBarColor(Activity activity, int statusColor) {
        if (VERSION.SDK_INT >= 21) {
            Window window = activity.getWindow();
            window.addFlags(Integer.MIN_VALUE);
            window.setStatusBarColor(statusColor);
        }
    }

    public static boolean isNullString(String string) {
        if (string == null) {
            return true;
        }
        try {
            if (string.trim().equalsIgnoreCase("null") || string.trim().length() < 0 || string.trim().equals(BuildConfig.FLAVOR)) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    public static Void displayToast(Context context, String strToast) {
        Toast.makeText(context, strToast, Toast.LENGTH_SHORT).show();
        return null;
    }

    public static boolean checkEmail(String email) {
        return !INVALID_EMAIL_PATTERN.matcher(email).matches() && EMAIL_PATTERN.matcher(email).matches();
    }

    public static boolean checkPassword(String password) {
        return PASSWORD_VALIDATION.matcher(password).matches();
    }

/*    public static boolean isInternetAvailable(Context context) {
        try {
            NetworkInfo netInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                return true;
            }
            displayToast(context, context.getString(R.string.str_no_internet));
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }*/

    public static boolean checkFirstLastName(String name) {
        return FIRST_LAST_NAME_PATTERN.matcher(name).matches();
    }

    public static void loadImage(Context context, String url, ImageView imageView) {
//        Glide.with(context).load(url).apply(new RequestOptions().placeholder(R.drawable.no_image)).into(imageView);
    }

    public static String dateFormatter(String dateFromJSON, String expectedFormat, String oldFormat) {
        String convertedDate = null;
        try {
            convertedDate = new SimpleDateFormat(expectedFormat).format(new SimpleDateFormat(oldFormat).parse(dateFromJSON));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public static void hideSoftKeyboard(Context context) {
        /*try {
            ((InputMethodManager) context.getSystemService("input_method")).hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    public static void rotateImage(float fromDegree, float toDegree, ImageView imgToRotate) {
        RotateAnimation rotateAnim = new RotateAnimation(fromDegree, toDegree, 1, 0.5f, 1, 0.5f);
        rotateAnim.setDuration(0);
        rotateAnim.setFillAfter(true);
        imgToRotate.startAnimation(rotateAnim);
    }

/*    public static int getDeviceHeight(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getRealSize(size);
        return size.y;
    }

    public static int getDeviceWidth(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getRealSize(size);
        return size.x;
    }*/

    public static void hideStatusBar(Activity activity) {
        try {
            activity.requestWindowFeature(1);
            activity.getWindow().setFlags(1024, 1024);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public static void loadImageFromUrlForFullPath(Context context, String url, ImageView view, ProgressBar prgbarProfileImageLoad, int reusetCode) {
        if (url != null && url != BuildConfig.FLAVOR) {
            view.setVisibility(0);
            Glide.with(context).load(BuildConfig.FLAVOR + url.replaceAll(" ", "%20").replaceAll("#", "%23")).listener(new RequestListener<Drawable>() {
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    return false;
                }
            }).into(view);
        }
    }*/

    public static float convertPixelsToDp(float px) {
        return (float) Math.round(px / (((float) Resources.getSystem().getDisplayMetrics().densityDpi) / 160.0f));
    }

    public static int convertDpToPixel(int dp) {
        return Math.round((float) ((int) (((float) dp) * (((float) Resources.getSystem().getDisplayMetrics().densityDpi) / 160.0f))));
    }

    /**
     * @purpose :method setFragment() handles redirection of Fragments.
     * @version : 1.0.0
     * @since : 1.0.0
     */
    public static void addFragmentTransaction(AppCompatActivity appCompatActivity, Fragment fragment, String tag) {
        if (fragment != null) {
            try {
                appCompatActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.container_main, fragment, tag)
                        .addToBackStack(tag)
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .setCustomAnimations(R.anim.fragment_slide_left_enter, R.anim.fragment_slide_right_exit)
                        .show(fragment)
                        .commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @purpose :method setFragment() handles redirection of Fragments.
     * @version : 1.0.0
     * @since : 1.0.0
     */
    public static void replaceTabContainerFragment(AppCompatActivity appCompatActivity, Fragment fragment, String tag) {
        if (fragment != null) {
            try {
                appCompatActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container_main, fragment, tag)
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @purpose : Method to show camera dialog picker to pick or click the image
     * @version : 1.0.0
     * @since : 1.0.0
     */
    public static void showCameraDialog(final Activity activity) {
        final Dialog dialogCamera = new Dialog(activity, R.style.DialogSlideAnim);
        dialogCamera.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogCamera.setCancelable(true);
        dialogCamera.setContentView(R.layout.dialog_camera_gallery);
        dialogCamera.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        dialogCamera.getWindow().setGravity(Gravity.BOTTOM);
        dialogCamera.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txt_take_photo = (TextView) dialogCamera.findViewById(R.id.txt_take_photo);
        TextView txt_gallery = (TextView) dialogCamera.findViewById(R.id.txt_gallery);
        TextView txt_cancel = (TextView) dialogCamera.findViewById(R.id.txt_cancel);

        txt_take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CameraGalleryImage.getCameraImage(activity, Constant.CAMERA_CODE);
                dialogCamera.dismiss();
            }
        });
        txt_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CameraGalleryImage.getGalleryImage(activity, Constant.GALLERY_CODE);
                dialogCamera.dismiss();
            }
        });
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogCamera.dismiss();
            }
        });
        dialogCamera.show();
    }
}
