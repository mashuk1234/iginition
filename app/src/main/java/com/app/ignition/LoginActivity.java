package com.app.ignition;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity implements OnClickListener {
    private static final String TAG = LoginActivity.class.getSimpleName();

    private TextView tvLogIn;
    private TextView tvForgotPassword;
    private RelativeLayout relSignup;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_tone);

        getSupportActionBar().setTitle(getResources().getString(R.string.signin));
        CommonUtils.hideSoftKeyboard(this);
        findViews();
        init();
        addListeners();
    }

    private void findViews() {
        relSignup = findViewById(R.id.relSignup);
        tvLogIn = findViewById(R.id.tvLogIn);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
    }

    private void init() {
    }

    private void addListeners() {
        tvForgotPassword.setOnClickListener(this);
        tvLogIn.setOnClickListener(this);
        relSignup.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.relSignup:
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
                break;

            case R.id.tvLogIn:
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
                break;

            case R.id.tvForgotPassword:
//                relSignup.performClick();
                break;

        }
    }

    private void validateSignUpData() {
//        if (CommonUtils.isNullString(this.mTInputEmail.getEditText().getText().toString().trim())) {
//            ToastUtil.show(this, getResources().getString(R.string.toast_empty_email));
//        } else if (!CommonUtils.checkEmail(this.mTInputEmail.getEditText().getText().toString().trim())) {
//            ToastUtil.show(this, getResources().getString(R.string.toast_invalid_email));
//        } else if (CommonUtils.isNullString(this.mTInputPassword.getEditText().getText().toString())) {
//            ToastUtil.show(this, getResources().getString(R.string.toast_empty_password));
//        } else if (CommonUtils.isInternetAvailable(this)) {
//            WSCustomerLogin(this.mTInputEmail.getEditText().getText().toString().trim(), this.mTInputPassword.getEditText().getText().toString().trim());
//        }
    }
}
