package com.app.ignition;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

public class RequestCVFragment extends Fragment implements View.OnClickListener {

    private Context mContext;
    private Button btnFreshers;
    private View btnExperience;
    private AdView mAdView;

    public static Fragment newInstance() {
        RequestCVFragment fragment = new RequestCVFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_request_cv, container, false);
        CommonUtils.hideSoftKeyboard(getActivity());
        setToolBar(rootView);
        findViews(rootView);
        init();
        return rootView;
    }

    private void setToolBar(View rootView) {
    }

    private void findViews(View rootView) {
        btnFreshers = rootView.findViewById(R.id.btnFreshers);
        btnExperience = rootView.findViewById(R.id.btnExperience);
        mAdView = (AdView) rootView.findViewById(R.id.adView);
        // initialize the AdMob app
        MobileAds.initialize(getActivity(), getString(R.string.admob_app_id));
        displayBannerAd();
    }

    private void init() {
        btnFreshers.setOnClickListener(this);
        btnExperience.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFreshers:
                CommonUtils.addFragmentTransaction((AppCompatActivity) mContext, CVDetailsFragment.newInstance(),
                        CVDetailsFragment.class.getSimpleName());
                break;
            case R.id.btnExperience:
                CommonUtils.addFragmentTransaction((AppCompatActivity) mContext, PaymentFragment.newInstance(),
                        PaymentFragment.class.getSimpleName());
                break;
        }
    }

    private void displayBannerAd() {
        AdRequest adRequest = new AdRequest.Builder()
                // If you want to show ad for specific device
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
//                .addTestDevice("C04B1BFFB0774708339BC273F8A43708")
                .build();
//        mAdView = (AdView) findViewById(R.id.adView);
//        mAdView.setAdSize(AdSize.BANNER);
//        mAdView.setAdUnitId(getString(R.string.banner_home_footer));
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(getActivity(), "Ad is closed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(getActivity(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                Toast.makeText(getActivity(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });
        mAdView.loadAd(adRequest);
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }
}